const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Create course
/*router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true){
	courseControllers.addCourse(req.body).then(resultFromController => {res.send(resultFromController);
		});
	}else{
		res.send("User is not authenticated");
	}
});
*/
// Create course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseControllers.addCourse(data).then(resultFromController => res.send(resultFromController));

});








// Retrieve all courses
router.get("/all", (req, res) => {

	courseControllers.getAllCourses().then(resultFromController => res.send(resultFromController));
})


/*
	Mini-Activity:
	1. Create a route that will retrieve ALL ACTIVE courses
	2. No need for user to login
	3. Create a controller that will return ALL ACTIVE courses
	4. Send your Postman output screenshot in our batch hangouts
*/

router.get("/active", (req, res) => {
	courseControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})


// retrieve a specific course
router.get("/:courseId", (req, res) =>{

	courseControllers.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// update a course
router.put("/:courseId", (req, res) => {

	courseControllers.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Archive course

router.put("/:courseId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true) {
	courseControllers.archiveCourse(req.params, req.body).then(resultFromController => {res.send(resultFromController);
});

}else{
	res.send("User is not authorized to commit changes")
}

});


/*router.put("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	
	courseControllers.archiveCourse(data).then(resultFromController => res.send(resultFromController));
});
*/


// export the router object for index.js file
module.exports = router;